const UPDATE_TODO_ITEM_EVENT = 'UPDATE-TODO-ITEM-EVENT';
const CHANGE_EDIT_STATUS = 'CHANGE-EDIT-STATUS';
const DEL_EVENT_ITEM = 'DEL-EVENT-ITEM';

const listReducer = (state, action) => {
	//todoList /////////////////////////////////
	switch ( action.type ) {
		case UPDATE_TODO_ITEM_EVENT:
			state[action.itemNumber - 1].event = action.text;
			return state;
		case CHANGE_EDIT_STATUS:
			const editStatus = state[action.itemNumber - 1].editStatus;

			state[action.itemNumber - 1].editStatus = !editStatus;
			return state;
		case DEL_EVENT_ITEM:
			state.splice(action.itemNumber - 1, 1);
			return state;
		default:
			return state;
	}
}

	export const updateTodoItemEventActionCreater = (itemNumber, text) => {
		const action = {
			type: UPDATE_TODO_ITEM_EVENT,
			itemNumber: itemNumber,
			text: text
		};
		return action;
	}
	
	export const changeEditStatusActionCreater = ( itemNumber ) => {
		const action = {
			type: CHANGE_EDIT_STATUS,
			itemNumber: itemNumber
		};
		return action;
	}
	
	export const delEventItemActionCreater = ( itemNumber ) => {
		const action = {
			type: DEL_EVENT_ITEM,
			itemNumber: itemNumber
		};
		return action;
	}
	
	export default listReducer;