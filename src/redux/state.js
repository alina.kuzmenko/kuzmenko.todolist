import listReducer from "./listReducer";
import blockReducer from "./blockReducer";

const store = {
  // properties
  _state: {
    listData: [
      { event: "Введите задание", editStatus: false },
      { event: "Введите задание", editStatus: false },
      { event: "Введите задание", editStatus: false }
    ],
    newTodoEvent: ""
  },

  // methods
  getState() {
    return this._state;
  },
  _callSubscriber(store) {
    console.log("Rerender");
  },
  subscribe(observer) {
    this._callSubscriber = observer;
  },

  dispatch(action) {
    listReducer(this._state.listData, action);
    blockReducer(this._state, action);

    this._callSubscriber(this);
  }
};

export default store;
