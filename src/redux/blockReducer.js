const blockReducer = (state, action) => {
  // todoBlock //////////////////////////////
  if (action.type === "ADD-TODO") {
    const sn = state.listData.length + 1;

    state.listData.push({
      event: state.newTodoEvent,
      editStatus: false
    });
    state.newTodoEvent = "";
  } else if (action.type === "UPDATE-EVENT-TEXT") {
    state.newTodoEvent = action.text;
  }

  return state;
};

export default blockReducer;
