import React from "react";
import s from "./TodoBlock.module.css";

const TodoBlock = props => {
  const todoInput = React.createRef();
  const newTodoEvent = props.newTodoEvent;

	const addTodo = () => {
		const action = {
			type:'ADD-TODO'
		};

		props.dispatch(action);
	};

	const updateEventText = () => {
		const action = {
			type:'UPDATE-EVENT-TEXT', 
			text: todoInput.current.value
		};

		props.dispatch(action);
	}

  return (
    <div className={s.block}>
      <input
        className="input"
        ref={todoInput}
        value={newTodoEvent}
        onChange={updateEventText}
      />
      <button className="addBtn" onClick={addTodo}>
        ADD
      </button>
    </div>
  );
};

export default TodoBlock;
