import React from "react";
import UserAction from "./UserAction/UserAction";

import s from "./ListItem.module.css";
import { updateTodoItemEventActionCreater } from "../../../redux/listReducer";



const ListItem = props => {
	const todoIput = React.createRef();
	const itemState = props.itemState;
	const itemNumber = props.sn;
	const editStatus = itemState.editStatus;
	const todoEvent = itemState.event;

	const updateTodoItemEvent = () => {
		props.dispatch( updateTodoItemEventActionCreater( itemNumber, todoIput.current.value ) );
	}

	return (
    <li className={s.item}>
      <div className={s.sn}>{itemNumber}</div>
			<input 
				ref={todoIput}
				type="text" 
				className={s.todo} 
				value={todoEvent}
				onChange={updateTodoItemEvent}
				disabled={!editStatus}
			/>
			<UserAction 
				itemNumber = {itemNumber}
				editStatus = {editStatus}
				dispatch={props.dispatch}
			/>
    </li>
  );
};

export default ListItem;
