import React from "react";
import s from "./UserAction.module.css";
import {
  changeEditStatusActionCreater,
  delEventItemActionCreater
} from "../../../../redux/listReducer";

const UserAction = props => {
  const itemNumber = props.itemNumber;
  const editStatus = props.editStatus;
  let btnText = editStatus ? "SAVE" : "EDIT";

  const changeEditStatus = () => {
    props.dispatch(changeEditStatusActionCreater(itemNumber));
  };

  const delEventItem = () => {
    props.dispatch(delEventItemActionCreater(itemNumber));
  };

  return (
    <div className={s.btns}>
      <button className="add" onClick={changeEditStatus}>
        {btnText}
      </button>
      <button className="del" onClick={delEventItem}>
        DEL
      </button>
    </div>
  );
};

export default UserAction;
