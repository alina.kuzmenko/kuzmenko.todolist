import React from "react";
import ListItem from "./ListItem/ListItem";

import s from "./TodoList.module.css";

const TodoList = props => {
	const itemsState = props.itemState;
	const updateTodoItemEvent = props.updateTodoItemEvent;
	const changeEditStatus = props.changeEditStatus;
	const delEventItem = props.delEventItem;

  const listEl = itemsState.map( (itemState, index) => (
		<ListItem 
			sn={index + 1}
			itemState={itemState} 
			dispatch={props.dispatch}
		/>
  ));

  return <ul className={s.list}>{listEl}</ul>;
};

export default TodoList;
