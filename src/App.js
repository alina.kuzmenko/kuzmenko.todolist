import React from "react";
import logo from "./logo.svg";
import "./App.css";

// our components
import TodoBlock from "./components/TodoBlock/TodoBlock";
import TodoList from "./components/TodoList/TodoList";

const App = props => {
	const state = props.state;
	
  return (
    <div className="App">
      <h1 className="title">Список заданий</h1>

			<TodoBlock 
				newTodoEvent={state.newTodoEvent} 
				dispatch={props.dispatch}
			/>

			<TodoList 
				itemState={state.listData} 
				dispatch={props.dispatch}
			/>
    </div>
  );
};

export default App;
